<?php
/*
 * Template Name: Welcome page
 * Description: A Page Template with a darker design.
 */

get_header('welcome'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


			<div id="hero-unit" class="desktop">

				<!-- HERO UNIT -->
				
				<?php
				
					$args = array('post_type'=>'hero', 'orderby'=>'rand', 'posts_per_page'=>'1');
					$hero = new WP_Query($args);
					
					while ($hero->have_posts()) : $hero->the_post(); 

						$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
				?>

						<article id="hero-<?php echo $post->ID ?>" class="entry-header hero-unit" style="background-image: url('<?php echo $img[0] ?>')">

							<div class="header-text container_12 clearfix">

							<div class="grid_6 push_3">
								<?php the_excerpt() ?>
								<h3 class="alexbrush"><?php the_title() ?></h3>
							</div>

						</article>
					
					<?php

					endwhile;
					wp_reset_postdata();

				?>

				<!-- END HERO UNIT -->

			</div>

			<!-- THE IDEA START -->
			<section id="the-idea">
				
				<div class="container_12 clearfix">
				
					<?php 
						$id = get_option('idea_id');
						$post = get_post($id); 
						$content = apply_filters('the_content', $post->post_content); 
					?>

					<div class="grid_12 suffix_1 prefix_1 clearfix">
					<h2><?php echo $post->post_title ?></h2>
					<?php echo $content; ?> 
					</div>
					<?php

						$children = get_pages(array(
							'child_of' => $id, 
							'orderby' => 'date', 
							'order' => 'ASC'
						) );

						if (count($children) != 0) {
							// var_dump($children);
						
							foreach ($children as $child) { ?>


							<div class="grid_4">
								<?php echo get_the_post_thumbnail( $child->ID, 'tumbnail'); ?> 
								<h4 class="microbrew"><?php echo $child->post_title ?></h4>
								<p><?php echo $child->post_content ?></p>
							</div>


						<?php
							}


						} else {
							echo "No child pages found";
						};

					?>

					<h3 class="alexbrush names"><?php echo get_option("signature"); ?></h3>
				
				</div>

			</section>
			<!-- THE IDEA END -->

			<!-- THE FOOD START -->
			<section id="the-food">
				
				<div class="container_12 clearfix">
					<div class="paper">
						<div class="grid_7 alpha food-menu">
							<h2 class="microbrew">Maten</h2>
							
							<?php

							/*

							$terms = get_terms('foodtype', array(
									'orderby' => 'slug',
									'hide_empty' => 0,
									'order' => 'ASC'
							) );

							// Loop the post types
							foreach ($terms as $term) {
								
								$args = array(
									'post_type' => 'food',
									'foodtype' => $term->slug,
									'order' => 'ASC'
								);

								$query = new WP_Query($args);

								echo'<h4>' . $term->name . '</h4>';
								echo '<ul class="food-listing">';
		 
								// Start the Loop
								while ( $query->have_posts() ) : $query->the_post(); ?>
								 
									<li class="food-item clearfix" id="post-<?php the_ID(); ?>">

										<div class="grid_8 alpha">
									    <strong><?php echo get_the_title(); ?></strong>
									    <em><?php echo get_the_excerpt(); ?></em>

									    <?php 

									    	$properties = get_field('food_properties'); 

									    	// Check if the food has any special properties
									    	if ($properties) {

									    		foreach ($properties as $property) {
									    			echo '<span class="food-property ' . $property . '"></span>';
									    		}

									    	}

									    ?>
									    </div>
									    <div class="grid_4 omega food_cost">
									    <span><?php the_field("price") ?> kr</span>
										</div>
									</li>
								 
								<?php endwhile;

								echo '</ul>';

							}
							*/

							/* STATIC FOOD MENU */

							?>

							<?php 
								$id = get_option('menu_id');
								$post = get_post($id); 
								$content = apply_filters('the_content', $post->post_content); 
							?>

							<div class="grid_12">
								<?php echo $content; ?> 
							</div>

						</div>

						<div class="grid_4 push_1 omega food-sidebar">
							<?php get_sidebar("menu") ?>
						</div>

						<div class="clearfix"></div>

					</div>

				</div>

			</section>
			<!-- THE FOOD END -->

			<!-- THE MAP START -->
			<section id="the-map" >
				
				<div class="container_12">
					<h2>Hitta till Utblick</h2>
				</div>

				<div id="directions" class="mobile">
					<div class="mobile-map"><a href="<?php echo get_option("map_link"); ?>">
						
					</a></div>

					<div class="container_12 center">
						<a href="<?php echo get_option("map_link"); ?>">Klicka på kartan för en vägbeskrivning</a>
					</div>
				</div>

				<div id="map" class="clearfix desktop"></div>

				<script>
					L.mapbox.accessToken = "pk.eyJ1IjoibG92ZWF0d29yayIsImEiOiJPV3poSUc4In0.9cqaYWaH6TsY4FF02Mi0qA";
					var map = L.mapbox.map('map', 'loveatwork.ifcbg84i');
					map.scrollWheelZoom.disable();
					map.touchZoom.disable();

				</script>

			</section>
			<!-- THE MAP END -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer('welcome'); ?>
