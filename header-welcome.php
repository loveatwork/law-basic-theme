<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package RestaurangUtblick
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<script src='https://api.tiles.mapbox.com/mapbox.js/v2.0.1/mapbox.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/v2.0.1/mapbox.css' rel='stylesheet' />
<link href='http://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>

<link href='<?php echo get_template_directory_uri(); ?>/inc/MyFontsWebfontsKit.css' rel='stylesheet' />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'restaurangutblick' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding container_12">
			<h1 class="site-title ir"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		</div>
	</header><!-- #masthead -->

	<div class="stickem-container">
	<nav id="site-navigation" class="main-navigation stickem" role="navigation">

		<div class="container_12 clearfix">
			<?php 

				$args = array(
					'theme_location' => 'primary',
					'container_id' => 'primary-nav'
				);

				wp_nav_menu( $args ); 

			?>
		</div>
	</nav><!-- #site-navigation -->

	<div id="content" class="site-content">
