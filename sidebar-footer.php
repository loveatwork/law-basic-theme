<?php
/**
 * The Sidebar containing the menu widget areas.
 *
 * @package RestaurangUtblick
 */
?>
<div id="secondary" class="widget-area widget-area-footer clearfix" role="complementary">

	<div class="grid_4">

		<img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.png" />

	</div>

	<?php if ( ! dynamic_sidebar( 'footer-sidebar' ) ) : ?>
	<?php endif; // end sidebar widget area ?>
</div><!-- #secondary -->
