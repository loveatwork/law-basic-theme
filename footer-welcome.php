<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package RestaurangUtblick
 */
?>
	</div>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer container_12" role="contentinfo">

		<?php get_sidebar('footer'); ?>

		<div class="site-info">
				<?php printf( __( 'Made in Luleå, Swedish Lapland. %1$s', 'restaurangutblick' ), '<a href="http://loveatwork.se/" class="ir madeby" rel="designer">LOVEATWORK</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<script src="http://code.jquery.com/jquery.js"></script>

<?php wp_footer(); ?>

</body>
</html>
