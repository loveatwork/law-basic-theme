<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package RestaurangUtblick
 */
?>

	</div><!-- #content -->

	<div class="container_12">
	<?php get_sidebar('footer'); ?>
	</div>
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'restaurangutblick' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'restaurangutblick' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( __( 'Theme: %1$s by %2$s.', 'restaurangutblick' ), 'RestaurangUtblick', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
