<?php
/**
 * RestaurangUtblick functions and definitions
 *
 * @package RestaurangUtblick
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'restaurangutblick_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function restaurangutblick_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on RestaurangUtblick, use a find and replace
	 * to change 'restaurangutblick' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'restaurangutblick', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'restaurangutblick' ),
	) );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link'
	) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'restaurangutblick_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// HERO UNIT POST TYPE
	add_action('init', 'cptui_register_my_cpt_hero');
	function cptui_register_my_cpt_hero() {
	register_post_type('hero', array(
	'label' => 'Hero units',
	'description' => '',
	'public' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'capability_type' => 'post',
	'map_meta_cap' => true,
	'hierarchical' => false,
	'rewrite' => array('slug' => 'hero', 'with_front' => true),
	'query_var' => true,
	'exclude_from_search' => true,
	'supports' => array('title','excerpt','revisions','thumbnail'),
	'labels' => array (
	  'name' => 'Hero units',
	  'singular_name' => 'Hero unit',
	  'menu_name' => 'Hero units',
	  'add_new' => 'Add Hero unit',
	  'add_new_item' => 'Add New Hero unit',
	  'edit' => 'Edit',
	  'edit_item' => 'Edit Hero unit',
	  'new_item' => 'New Hero unit',
	  'view' => 'View Hero unit',
	  'view_item' => 'View Hero unit',
	  'search_items' => 'Search Hero units',
	  'not_found' => 'No Hero units Found',
	  'not_found_in_trash' => 'No Hero units Found in Trash',
	  'parent' => 'Parent Hero unit',
	)
	) ); }

	// FOOD CUSTOM POST TYPE
	add_action('init', 'cptui_register_my_cpt_food');
	function cptui_register_my_cpt_food() {
	register_post_type('food', array(
	'label' => 'Food',
	'description' => '',
	'public' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'capability_type' => 'post',
	'map_meta_cap' => true,
	'hierarchical' => true,
	'rewrite' => array('slug' => 'food', 'with_front' => true),
	'query_var' => true,
	'supports' => array('title','excerpt','thumbnail'),
	'labels' => array (
	  'name' => 'Food',
	  'singular_name' => 'Food',
	  'menu_name' => 'Food',
	  'add_new' => 'Add Food',
	  'add_new_item' => 'Add New Food',
	  'edit' => 'Edit',
	  'edit_item' => 'Edit Food',
	  'new_item' => 'New Food',
	  'view' => 'View Food',
	  'view_item' => 'View Food',
	  'search_items' => 'Search Food',
	  'not_found' => 'No Food Found',
	  'not_found_in_trash' => 'No Food Found in Trash',
	  'parent' => 'Parent Food',
	)
	) ); }

	// FOOD TYPE TAXONOMY
	add_action('init', 'cptui_register_my_taxes_foodtype');
	function cptui_register_my_taxes_foodtype() {
	register_taxonomy( 'foodtype',array (
	  0 => 'food',
	),
	array( 'hierarchical' => true,
		'label' => 'Food types',
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => false,
		'labels' => array (
	  'search_items' => 'Food type',
	  'popular_items' => '',
	  'all_items' => '',
	  'parent_item' => '',
	  'parent_item_colon' => '',
	  'edit_item' => '',
	  'update_item' => '',
	  'add_new_item' => '',
	  'new_item_name' => '',
	  'separate_items_with_commas' => '',
	  'add_or_remove_items' => '',
	  'choose_from_most_used' => '',
	)
	) ); 
	}

	// FOOD PROPERTIES
	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_food',
			'title' => 'Food',
			'fields' => array (
				array (
					'key' => 'field_539ed02cbe886',
					'label' => 'Om maten',
					'name' => 'food_properties',
					'type' => 'checkbox',
					'choices' => array (
						'gluten_free' => 'Glutenfri',
						'lactose_free' => 'Laktosfri',
						'contains_nuts' => 'Innehåller nötter',
					),
					'default_value' => '',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_539feb69a025b',
					'label' => 'Pris',
					'name' => 'price',
					'type' => 'number',
					'required' => 1,
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'kr',
					'min' => '',
					'max' => '',
					'step' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'food',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}

	// Register sidebars
	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'name' => 'Menu sidebar',
			'id' => 'menu-sidebar',
			'description' => 'Sidebar next to menu',
			'before_widget' => '<div class="clearfix widget" id="%1$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
		));

		register_sidebar(array(
			'name' => 'Footer sidebar',
			'id' => 'footer-sidebar',
			'description' => 'The footer sidebar',
			'before_widget' => '<div class="grid_4">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
		));
	}


}
endif; // restaurangutblick_setup
add_action( 'after_setup_theme', 'restaurangutblick_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function restaurangutblick_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'restaurangutblick' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'restaurangutblick_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function restaurangutblick_scripts() {
	wp_enqueue_style( 'restaurangutblick-style', get_stylesheet_uri() );
	wp_enqueue_script('restaurangutblick-all', get_template_directory_uri() . "/js/utblick.min.js", array(), "20140616", true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'restaurangutblick_scripts' );

function add_loveatwork_global_settings() {
	add_options_page('Loveatwork settings', 'Loveatwork settings', 'manage_options', 'functions','loveatwork_global_settings');
}

// Add settings page with global variables
function loveatwork_global_settings() {
?>
    <div class="wrap">
        <h2>Loveatwork settings</h2>
        <form method="post" action="options.php">
            <?php wp_nonce_field('update-options') ?>
            

            <h3>Kontaktuppgifter</h3>
            <p><strong>Telefonnr:</strong><br />
                <input type="text" name="contact_phone" size="45" value="<?php echo get_option('contact_phone'); ?>" />
            </p>

            <p><strong>E-post:</strong><br />
                <input type="text" name="contact_email" size="45" value="<?php echo get_option('contact_email'); ?>" />
            </p>

            <p><strong>Facebooksida:</strong><br />
                <input type="text" name="fb_link" size="45" value="<?php echo get_option('fb_link'); ?>" />
            </p>

            <h3>Övrigt</h3>
			<p><strong>Vår idé (ID)</strong><br />
                <input type="text" name="idea_id" size="45" value="<?php echo get_option('idea_id'); ?>" />
            </p>

            <p><strong>Meny (ID)</strong><br />
                <input type="text" name="menu_id" size="45" value="<?php echo get_option('menu_id'); ?>" />
            </p>

            <p><strong>Signatur</strong><br />
                <input type="text" name="signature" size="45" value="<?php echo get_option('signature'); ?>" />
            </p>

            <p><strong>Kartlänk</strong><br />
                <input type="text" name="map_link" size="45" value="<?php echo get_option('map_link'); ?>" />
            </p>


            <p><input type="submit" name="Submit" value="Store Options" /></p>
            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="contact_phone,contact_email,fb_link,idea_id,menu_id,signature,map_link" />
        </form>
    </div>

<?php
}

add_action('admin_menu', 'add_loveatwork_global_settings');

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
