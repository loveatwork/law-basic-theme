<?php
/**
 * The Sidebar containing the menu widget areas.
 *
 * @package RestaurangUtblick
 */
?>
<div id="secondary" class="widget-area widget-area-menu clearfix" role="complementary">
	<?php if ( ! dynamic_sidebar( 'menu-sidebar' ) ) : ?>
	<?php endif; // end sidebar widget area ?>
</div><!-- #secondary -->
